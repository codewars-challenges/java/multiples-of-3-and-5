package be.hics.sandbox.multiplesof3and5;

import java.util.function.IntPredicate;
import java.util.stream.IntStream;

/**
 * https://www.codewars.com/kata/multiples-of-3-or-5/java
 * Kata
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Finish the solution so that it returns the sum of all the multiples of 3 or 5 below the number passed in.
 * Note: If the number is a multiple of both 3 and 5, only count it once.
 */
public class Solution {

    private static IntPredicate multipleOf3Or5 = i -> i % 3 == 0 || i % 5 == 0;

    public int solution(int number) {
        return IntStream.range(1, number).filter(multipleOf3Or5).distinct().sum();
    }

}
