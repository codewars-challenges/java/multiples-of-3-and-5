package be.hics.sandbox.multiplesof3and5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiplesOf3And5Application {

	public static void main(String[] args) {
		SpringApplication.run(MultiplesOf3And5Application.class, args);
        System.out.println(new Solution().solution(10));
	}
}
